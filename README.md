# Scale FeatherWing  
Scale FeatherWing open source pcb with RC input filter and dedicated LDO for load cell excitation. (ADS1232 24-bit ADC)


![Scale FeatherWing](Photos/feather.jpg)
![Scale FeatherWing](Photos/feather2.jpg)
  
  

[![](http://img.youtube.com/vi/he4om1_fZbk/0.jpg)](https://youtu.be/he4om1_fZbk "")

  

If you need a full featured plug and play PCB check out espresso-scale:  
https://gitlab.com/jousis/espresso-scale


 
v2 (testing)  
 - simplified BOM with roughly the same resolution as v1

**License**  
Software under [LGPL v3](https://gitlab.com/jousis/scale-featherwing/blob/master/LICENSE)  
Hardware under [CERN Open Hardware Licence v1.2](https://gitlab.com/jousis/scale-featherwing/blob/master/hw-LICENSE)  
Documentation under [CC BY-SA license 2.0](https://creativecommons.org/licenses/by-sa/2.0/)