


#include "ADS1232.h"
#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>





//Note:
//If you want to compile it for other feathers, simply change the Pin numbers
//Especially for ESP32, (optional) go to the library and uncomment the line #define ESP32


const uint8_t ADC_PDWN_PIN = 7;
const uint8_t ADC_DOUT_PIN = 16;
const uint8_t ADC_SCLK_PIN = 15;
const uint8_t ADC_SPEED_PIN = A5;
const uint8_t ADC_GAIN0_PIN = 0; //disabled
const uint8_t ADC_GAIN1_PIN = 0; //disabled
const uint8_t ADC_A0_PIN = 0; //disabled
const uint8_t ADC_TEMP_PIN = 0; //disabled

ADS1232 adc = ADS1232(ADC_PDWN_PIN, ADC_SCLK_PIN, ADC_DOUT_PIN, ADC_A0_PIN, ADC_SPEED_PIN, ADC_GAIN1_PIN, ADC_GAIN0_PIN, ADC_TEMP_PIN);

const uint8_t BUTTON_A = 31;
const uint8_t BUTTON_B = 30;
const uint8_t BUTTON_C = 27;

Adafruit_SSD1306 display = Adafruit_SSD1306(128, 32, &Wire);


uint8_t adc_speed = 10;

uint32_t tare_value = 5000;
float cal_factor = 4800.0;

uint32_t last_display_update = 0;
uint32_t display_update_interval = 50; //update every 100ms = 10fps, 50ms = 20fps


void setup() {
  Serial.begin(115200);
  display.begin(SSD1306_SWITCHCAPVCC, 0x3C); // Address 0x3C for 128x32
  display.clearDisplay();
  display.display();

  int32_t useless_var = 0;
  
    
  delay(1000);
  pinMode(BUTTON_A, INPUT_PULLUP);
  pinMode(BUTTON_B, INPUT_PULLUP);
  pinMode(BUTTON_C, INPUT_PULLUP);
  display.setTextSize(1);
  display.setTextColor(WHITE);
  display.setTextSize(1);
  display.setCursor(45, 0);
  display.print("Scale");
  display.setCursor(30, 16);
  display.print("FeatherWing");
  display.display();

  adc.begin(0, 128, adc_speed);
  uint32_t timeout = millis() + 1000;
  while (timeout < millis()) {
    useless_var = adc.readRaw(1);
    yield();
  }
  
  adc.calibrateADC();

  
  timeout = millis() + 1000;
  while (timeout < millis()) {
    useless_var = adc.readRaw(1);
    yield();
  }

  tare_value = adc.readRaw(10);

}

void loop() {

  int32_t newvalue = adc.readRaw(1);
  if (!digitalRead(BUTTON_A)) {
    display.clearDisplay();
    display.setCursor(0, 12);
    display.setTextSize(2);
    display.print("TARE OK");
    display.display();
    tare_value = newvalue;
    Serial.print("tare = "); Serial.println(tare_value);
    delay(2000);
  }
  if (!digitalRead(BUTTON_B)) {
    display.clearDisplay();
    display.setCursor(30, 0);
    display.setTextSize(2);
    display.print("CAL TO");
    display.setCursor(20, 16);
    display.print("100g OK");
    cal_factor = (newvalue - tare_value) / 100.0;
    Serial.print("cal_factor = "); Serial.println(cal_factor);
    display.display();
    delay(2000);
  }

  if (!digitalRead(BUTTON_C)) {
    display.clearDisplay();
    display.setCursor(0, 12);
    display.setTextSize(2);
    if (adc_speed == 10) {
      adc_speed = 80;
    } else {
      adc_speed = 10;
    }
    display.print("SPEED = "); display.print(adc_speed);
    adc.setSpeed(adc_speed);
    display.display();
    delay(2000);
  }

  int32_t tared_value = newvalue - tare_value;
  
  if (millis() - last_display_update > display_update_interval) {
    double weight = (double)tared_value / (double)cal_factor;
    double mlt = powf( 10.0f, 2 );
    weight = roundf( weight * mlt ) / mlt;  
    last_display_update += display_update_interval;
    if (weight == -0.0) { weight = 0.0; }
    Serial.println(weight);
    display.clearDisplay();
    display.setCursor(0, 10);
    display.setTextSize(2);
    display.setTextColor(WHITE);
    display.setCursor(12, 10);
    display.print(weight);display.print(" g");
    //delay(10);
    display.display();
  }


}
